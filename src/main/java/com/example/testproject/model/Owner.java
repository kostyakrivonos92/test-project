package com.example.testproject.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Owner {

    private UUID id;

    private String name;

    private String lastname;

    private List<Car> carList = new ArrayList<>();
}

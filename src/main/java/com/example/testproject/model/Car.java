package com.example.testproject.model;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Car {

    private UUID id;

    private String brand;

    private Owner owner;
}

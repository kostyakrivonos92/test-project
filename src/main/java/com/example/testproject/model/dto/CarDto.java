package com.example.testproject.model.dto;

import com.example.testproject.model.Owner;
import lombok.*;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarDto {

    private UUID id;

    private String brand;

    private OwnerDto owner;
}

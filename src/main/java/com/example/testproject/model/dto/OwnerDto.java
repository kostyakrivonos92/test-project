package com.example.testproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OwnerDto {

    private UUID id;

    private String name;

    private String lastname;

    private List<CarDto> carList = new ArrayList<>();

}

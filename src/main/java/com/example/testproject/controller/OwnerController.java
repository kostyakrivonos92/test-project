package com.example.testproject.controller;

import com.example.testproject.mapper.OwnerMapper;
import com.example.testproject.model.dto.OwnerDto;
import com.example.testproject.service.OwnerServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/owner")
public class OwnerController {

    private final OwnerServiceImpl ownerService;

    private final OwnerMapper ownerMapper;

    @GetMapping
    public List<OwnerDto> getAll() {
        return ownerMapper.ownerListToOwnerDtoList(ownerService.getAll());
    }

    @GetMapping("/{lastname}")
    public OwnerDto getByLastName(@PathVariable String lastname) {
        return ownerMapper.ownerToOwnerDto(ownerService.getByLastname(lastname));
    }

}

package com.example.testproject.service;

import com.example.testproject.model.Owner;

import java.util.List;

public interface OwnerService {

    List<Owner> getAll();

    Owner getByLastname(String lastname);

}

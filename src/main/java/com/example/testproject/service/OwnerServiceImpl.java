package com.example.testproject.service;

import com.example.testproject.model.Car;
import com.example.testproject.model.Owner;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class OwnerServiceImpl implements OwnerService{

    public List<Owner> getAll() {
        return getInitOwner();
    }

    public Owner getByLastname(String lastname) {
        return getInitOwner().stream()
            .filter(owner -> owner.getLastname().equals(lastname))
            .findFirst().get();
    }

    private List<Owner> getInitOwner() {
        Owner petrenko = Owner.builder()
                             .id(UUID.randomUUID())
                             .lastname("Petrenko")
                             .build();

        Owner sternenko = Owner.builder()
                             .id(UUID.randomUUID())
                             .lastname("Sternenko")
                             .build();

        Owner fedorenko = Owner.builder()
                             .id(UUID.randomUUID())
                             .lastname("Fedorenko")
                             .build();

        Car audi = Car.builder()
                       .id(UUID.randomUUID())
                       .brand("Audi")
                       .owner(petrenko)
                       .build();
        Car bmw = Car.builder()
                      .id(UUID.randomUUID())
                      .brand("BMW")
                      .owner(sternenko)
                      .build();
        Car mercedes = Car.builder()
                           .id(UUID.randomUUID())
                           .brand("Mercedes")
                           .owner(fedorenko)
                           .build();
        Car nissan = Car.builder()
                        .id(UUID.randomUUID())
                        .brand("Nissan")
                        .owner(fedorenko)
                        .build();

        petrenko.setCarList(Arrays.asList(audi));
        sternenko.setCarList(Arrays.asList(bmw));
        fedorenko.setCarList(Arrays.asList(mercedes, nissan));

        return Arrays.asList(petrenko, sternenko, fedorenko);
    }

}

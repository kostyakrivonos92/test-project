package com.example.testproject.mapper;

import com.example.testproject.model.Car;
import com.example.testproject.model.Owner;
import com.example.testproject.model.dto.CarDto;
import com.example.testproject.model.dto.OwnerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface OwnerMapper {

    OwnerDto ownerToOwnerDto(Owner owner);

    @Mapping(target = "owner", ignore = true)
    CarDto carToCarDto(Car car);

    List<OwnerDto> ownerListToOwnerDtoList(List<Owner> ownerList);

    List<CarDto> carListToCarDtoList(List<Car> carList);

}
